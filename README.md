

# Usage

```$xslt
import Vue from 'vue';
import Vuex from 'vuex';

/** Import plugin factory **/
import { createStatePlugin } from "vue-app-state";

/**
 * Define your 2-way, 1-way and 2-way async bindings
 * @type {{twoWay: string[], oneWay: string[], dispatching: Array}}
 */
const stateComputes = {
	//2-way bindings, Must have getter and mutation with same name to get/set this prop
	twoWay: [
		'industry',
	],
	//1-way/ read-only bindings. Must have getter with same name to use to get value.
	oneWay: [
		'hi'
	],
	//2-way async bindings. Must have a getter with same name to get value and an action of same name to dispatch when updated
	dispatching: [
	]
};

/** Create and use plugin **/
const statePlugin = createStatePlugin(stateComputes);
Vue.use( statePlugin );

/** Example Vuex store to use with this plugin */
const store =  new Vuex.Store({
	strict: false,
	plugins: [],
	modules: {},
	state: {
		industry: 'Apex',
		hi: 'Roy'
	},
	getters: {
		industry: state => {
			return state.foo;
		},
		bar: state => {
			return state.bar;
		}
	},
	mutations: {
		industry(state, value ){
			state.industry = value;
		}
	},
	actions: {}
});

export default store;
```