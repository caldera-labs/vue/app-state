import {
	StateFactory,
	StateFactoryDispatching,
	StateFactoryReadOnly
} from "./factories";

/**
 * Creates a VueJS plugin that binds state form Vuex store automatically.
 *
 * @param stateComputes {Object}
 * @returns {{install: (function(*, *): void)}}
 */
export function createStatePlugin(stateComputes) {
	let computed = {};
	/** Inject two-way bindings **/
	stateComputes.twoWay.forEach( (key,i) => {
		computed[key] = StateFactory( key, key );
	});

	/** Inject one-way bindings **/
	stateComputes.oneWay.forEach( (key,i) => {
		computed[key] = StateFactoryReadOnly( key, key );
	});

	/** Inject two-way, asynchronous bindings **/
	stateComputes.dispatching.forEach( (key,i) => {
		computed[key] = StateFactoryDispatching( key, key );
	});

	return{
		install(Vue, options) {
			Vue.mixin({
				computed: computed
			});
		}
	}

}
